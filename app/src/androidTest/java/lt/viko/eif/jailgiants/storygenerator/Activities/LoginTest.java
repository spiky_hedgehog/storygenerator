package lt.viko.eif.jailgiants.storygenerator.Activities;

import static org.junit.Assert.assertNotNull;

import android.view.View;
import androidx.test.rule.ActivityTestRule;
import lt.viko.eif.jailgiants.storygenerator.R;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

public class LoginTest {

  @Rule
  public ActivityTestRule<Login> mActivityTestRule = new ActivityTestRule <Login>(Login.class);
  private Login mActivity = null;

  @Before
  public void setUp() throws Exception {
    mActivity = mActivityTestRule.getActivity();
  }

  @Test
  public void onCreateTest(){
    View loginTopTextView = mActivity.findViewById(R.id.loginTopTextView);
    View emailAddress = mActivity.findViewById(R.id.emailAddress);
    View password = mActivity.findViewById(R.id.password);
    View loginButton = mActivity.findViewById(R.id.loginButton);
    View googleBtn = mActivity.findViewById(R.id.googleBtn);
    View login_button = mActivity.findViewById(R.id.login_button);
    View goToRegister = mActivity.findViewById(R.id.login_button);
    View loginProgressBar = mActivity.findViewById(R.id.loginProgressBar);
    assertNotNull(loginTopTextView);
    assertNotNull(emailAddress);
    assertNotNull(password);
    assertNotNull(loginButton);
    assertNotNull(googleBtn);
    assertNotNull(login_button);
    assertNotNull(goToRegister);
    assertNotNull(loginProgressBar);
  }


  @After
  public void tearDown() throws Exception {
    mActivity = null;
  }


}

