package lt.viko.eif.jailgiants.storygenerator.Fragments;

import static org.junit.Assert.*;

import android.view.View;
import androidx.test.rule.ActivityTestRule;
import lt.viko.eif.jailgiants.storygenerator.Activities.LoginSetup;
import lt.viko.eif.jailgiants.storygenerator.Activities.MainActivity;
import lt.viko.eif.jailgiants.storygenerator.R;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({LoginSetup.class})
public class MyStoriesTest {

  @Rule
  public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule <MainActivity>(MainActivity.class);
  private MainActivity mActivity = null;

  @Before
  public void setUp() throws Exception {
    mActivity = mActivityTestRule.getActivity();

  }

  @Test
  public void onCreateTest() {
    View recyclerView = mActivity.findViewById(R.id.recyclerView);
    View textView4 = mActivity.findViewById(R.id.textView4);
    View textView5 = mActivity.findViewById(R.id.textView5);
    assertNotNull(recyclerView);
    assertNotNull(textView4);
    assertNotNull(textView5);
  }

  @After
  public void tearDown() throws Exception {
    mActivity = null;
  }

}