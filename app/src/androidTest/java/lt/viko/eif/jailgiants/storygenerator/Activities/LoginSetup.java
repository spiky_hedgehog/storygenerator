package lt.viko.eif.jailgiants.storygenerator.Activities;

import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import java.io.IOException;

import lt.viko.eif.jailgiants.storygenerator.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

@LargeTest
public class LoginSetup {

  @Rule
  public ActivityTestRule<Login> mLoginActivity = new ActivityTestRule<>(Login.class);

  @Test
  public void testSetup() throws IOException {

    onView(withId(R.id.emailAddress)).perform(replaceText("test@test.com"));
    onView(withId(R.id.password)).perform(replaceText("test123"));
    onView(withId(R.id.login_button)).perform(click());

  }

}
