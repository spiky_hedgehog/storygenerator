package lt.viko.eif.jailgiants.storygenerator.Activities;

import static org.junit.Assert.*;

import android.view.View;
import androidx.test.rule.ActivityTestRule;
import lt.viko.eif.jailgiants.storygenerator.R;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

public class RegisterTest {


  @Rule
  public ActivityTestRule<Register> mActivityTestRule = new ActivityTestRule <Register>(Register.class);
  private Register mActivity = null;

  @Before
  public void setUp() throws Exception {
    mActivity = mActivityTestRule.getActivity();
  }

  @Test
  public void onCreateTest(){
    View registerTopTextView = mActivity.findViewById(R.id.registerTopTextView);
    View fullName = mActivity.findViewById(R.id.fullName);
    View emailAddress = mActivity.findViewById(R.id.emailAddress);
    View password = mActivity.findViewById(R.id.password);
    View registerButton = mActivity.findViewById(R.id.registerButton);
    View goToLogin = mActivity.findViewById(R.id.goToLogin);
    View registerProgressBar = mActivity.findViewById(R.id.registerProgressBar);
    assertNotNull(registerTopTextView);
    assertNotNull(fullName);
    assertNotNull(emailAddress);
    assertNotNull(password);
    assertNotNull(registerButton);
    assertNotNull(goToLogin);
    assertNotNull(registerProgressBar);
  }


  @After
  public void tearDown() throws Exception {
    mActivity = null;
  }



}