package lt.viko.eif.jailgiants.storygenerator.Activities;

import static org.junit.Assert.*;

import android.view.View;
import androidx.test.rule.ActivityTestRule;
import lt.viko.eif.jailgiants.storygenerator.R;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({LoginSetup.class})
public class PlayActivityTest {
  
  @Rule
  public ActivityTestRule<PlayActivity> mActivityTestRule = new ActivityTestRule <PlayActivity>(PlayActivity.class);
  private PlayActivity mActivity = null;

  @Before
  public void setUp() throws Exception {
    mActivity = mActivityTestRule.getActivity();
  }

  @Test
  public void onCreateTest(){
    View bottomNavigationView = mActivity.findViewById(R.id.bottomNavigationView);
    View frameLayout = mActivity.findViewById(R.id.frameLayout);
    View frame = mActivity.findViewById(R.id.frame);
    assertNotNull(bottomNavigationView);
    assertNotNull(frameLayout);
    assertNotNull(frame);
  }


  @After
  public void tearDown() throws Exception {
    mActivity = null;
  }

}