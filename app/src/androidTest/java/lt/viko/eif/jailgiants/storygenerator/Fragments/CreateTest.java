package lt.viko.eif.jailgiants.storygenerator.Fragments;

import static org.junit.Assert.*;

import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.test.rule.ActivityTestRule;
import java.util.Objects;
import lt.viko.eif.jailgiants.storygenerator.Activities.Login;
import lt.viko.eif.jailgiants.storygenerator.Activities.LoginSetup;
import lt.viko.eif.jailgiants.storygenerator.Activities.MainActivity;
import lt.viko.eif.jailgiants.storygenerator.Activities.Register;
import lt.viko.eif.jailgiants.storygenerator.R;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({LoginSetup.class})
public class CreateTest {


  @Rule
  public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule <MainActivity>(MainActivity.class);
  private MainActivity mActivity = null;

  @Before
  public void setUp() throws Exception {
    mActivity = mActivityTestRule.getActivity();

  }

  @Test
  public void onCreateTest() {
    View titleText = mActivity.findViewById(R.id.titleText);
    View textCreateView = mActivity.findViewById(R.id.textCreateView);
    View button_save = mActivity.findViewById(R.id.button_save);
    View buttonsLayout = mActivity.findViewById(R.id.buttonsLayout);
    View button_add_1 = mActivity.findViewById(R.id.button_add_1);
    View DropDown = mActivity.findViewById(R.id.DropDown);
    View SplitCheck = mActivity.findViewById(R.id.SplitCheck);
    View radioContainer = mActivity.findViewById(R.id.radioContainer);
    View RadioLeft = mActivity.findViewById(R.id.RadioLeft);
    View RadioRight = mActivity.findViewById(R.id.RadioRight);

    assertNotNull(titleText);
    assertNotNull(textCreateView);
    assertNotNull(button_save);
    assertNotNull(buttonsLayout);
    assertNotNull(button_add_1);
    assertNotNull(DropDown);
    assertNotNull(SplitCheck);
    assertNotNull(radioContainer);
    assertNotNull(RadioLeft);
    assertNotNull(RadioRight);
  }

  @After
  public void tearDown() throws Exception {
    mActivity = null;
  }
}