package lt.viko.eif.jailgiants.storygenerator.Models;

import static org.junit.Assert.*;

import java.util.ArrayList;
import org.junit.Test;

public class StoryTest {

  @Test
  public void testStoryModel(){

    StoryFragment testFragment = new StoryFragment("fakeContent", 1, 2, 12);
    StoryFragment testFragment2 = new StoryFragment("fakeContent2", 12, 22, 122);
    ArrayList<StoryFragment> testList = new ArrayList<StoryFragment>();
    testList.add(testFragment);
    testList.add(testFragment2);

    Story testStory = new Story("du", "testTitle", "test@test.com", testList);
    Story empty = new Story();
    empty.setId("id-1");
    empty.setTitle("pav");
    empty.setUserEmail("bb@bb");
    empty.setStoryFragments(testList);
    assert (testStory.getTitle().equals("testTitle"));
    assert (testStory.getId().equals("du"));
    assert (testStory.getUserEmail().equals("test@test.com"));
    assert (empty.getId().equals("id-1"));
    assert (empty.getTitle().equals("pav"));
    assert (empty.getUserEmail().equals("bb@bb"));
    assert (empty.getStoryFragments().contains(testFragment));
  }

}