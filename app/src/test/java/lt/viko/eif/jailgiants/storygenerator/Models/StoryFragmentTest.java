package lt.viko.eif.jailgiants.storygenerator.Models;

import static org.junit.Assert.*;

import org.junit.Test;

public class StoryFragmentTest {

  @Test
  public void storyFragmentTest(){
    StoryFragment testFragment = new StoryFragment("fakeContent", 1, 2, 12);
    StoryFragment emptyFragment = new StoryFragment();
    emptyFragment.setContent("aaa");
    emptyFragment.setLeft(11);
    emptyFragment.setRight(101);
    emptyFragment.setPosition(999);

    assert (testFragment.getContent().equals("fakeContent"));
    assert (testFragment.getLeft()==1);
    assert (testFragment.getRight()==2);
    assert (testFragment.getPosition()==12);
    assert (emptyFragment.getContent().equals("aaa"));
    assert (emptyFragment.getLeft()==11);
    assert (emptyFragment.getRight()==101);
    assert (emptyFragment.getPosition()==999);
  }

}