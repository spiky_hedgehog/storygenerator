package lt.viko.eif.jailgiants.storygenerator.Models;

import java.io.Serializable;

public class StoryFragment implements Serializable {

  private String content;
  private int position;
  private int left;
  private int right;

  public StoryFragment() {
  }

  public StoryFragment(String content, int left, int right, int position) {
    this.content = content;
    this.left = left;
    this.right = right;
    this.position = position;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public int getLeft() {
    return left;
  }

  public void setLeft(int left) {
    this.left = left;
  }

  public int getRight() {
    return right;
  }

  public void setRight(int right) {
    this.right = right;
  }

  public int getPosition() {
    return position;
  }

  public void setPosition(int position) {
    this.position = position;
  }
}
