package lt.viko.eif.jailgiants.storygenerator.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import java.util.ArrayList;
import java.util.List;

import lt.viko.eif.jailgiants.storygenerator.Activities.MainActivity;
import lt.viko.eif.jailgiants.storygenerator.Models.StoryFragment;
import lt.viko.eif.jailgiants.storygenerator.R;
import lt.viko.eif.jailgiants.storygenerator.Repositories.StoryRepository;

public class Create extends Fragment {

  private ArrayList<StoryFragment> fragmentList = new ArrayList<>();
  private ArrayList<StoryFragment> leafsList = new ArrayList<>();
  private EditText title;
  private EditText fragmentText;
  private RadioButton buttonLeft, buttonRight;
  private int maxId = 0;
  private int dropdownSelection=0;
  private boolean isCheck = false;
  private ArrayList<String> dropItems = new ArrayList<>();
  private ArrayAdapter<String> dropAdapter;
  private RadioGroup radioBox;
  private boolean addedOne=false;
  private String titleString;
  private String fragmentTextString;
  StoryFragment splitParent;

  public Create() {
  }


  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {

    dropAdapter = new ArrayAdapter<>(getContext(),R.layout.spinner_list_item,R.id.spiinertxt,dropItems);
    final View view = inflater.inflate(R.layout.fragment_create, container, false);

    Button saveButton = view.findViewById(R.id.button_save);
    final Button addOne = view.findViewById(R.id.button_add_1);
    final CheckBox check = view.findViewById(R.id.SplitCheck);
    final Spinner dropdown = view.findViewById(R.id.DropDown);
    radioBox = view.findViewById(R.id.radioContainer);
    buttonLeft = view.findViewById(R.id.RadioLeft);
    buttonRight = view.findViewById(R.id.RadioRight);
    title = view.findViewById(R.id.titleText);
    fragmentText = view.findViewById(R.id.textCreateView);
    check.setEnabled(false);





    dropdown.setAdapter(dropAdapter);
    dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        dropdownSelection=position;
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {
        dropdownSelection=0;
      }
    });


    check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
          isCheck = true;
          buttonLeft.setEnabled(true);
          buttonRight.setEnabled(true);
        } else {
          isCheck = false;
          buttonLeft.setEnabled(false);
          buttonRight.setEnabled(false);
          buttonRight.setChecked(false);
          buttonLeft.setChecked(false);
        }
      }
    });



    buttonLeft.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        buttonRight.setChecked(false);
      }
    });

    buttonRight.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        buttonLeft.setChecked(false);
      }
    });

    saveButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        buttonLeft.setEnabled(false);
        buttonRight.setEnabled(false);
        if (!fragmentList.isEmpty()) {
          titleString = title.getText().toString();
          StoryRepository.saveNewStory(titleString, fragmentList);
          fragmentList = new ArrayList<>();
          maxId = 0;
        } else {
          Toast.makeText(view.getContext(), "Cannot save an empty story", Toast.LENGTH_LONG).show();
        }
      }
    });

    addOne.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View v) {

        fragmentTextString = fragmentText.getText().toString();
        if (!isCheck) {
          addSingle();
          check.setEnabled(true);
        }
        else{

          if(!buttonRight.isChecked()&&!buttonLeft.isChecked())
          {
            Toast.makeText(view.getContext(), "Cannot add card without choosing split direction",
                    Toast.LENGTH_LONG).show();
          }

          if(!addedOne)
          {
            splitParent = getParentSelected(dropdownSelection);
            dropAdapter.clear();
            getLeafs(fragmentList);
            dropAdapter.notifyDataSetChanged();

            addSplit(splitParent);


            buttonLeft.setChecked(buttonRight.isChecked());
            buttonRight.setChecked(!buttonLeft.isChecked());
            addedOne=true;
          }
          else
          {

            System.out.println("ADDING 2ND SPLIT CARD ???");
            dropAdapter.clear();
            getLeafs(fragmentList);
            dropAdapter.notifyDataSetChanged();

            addSplit(splitParent);
            addedOne=false;
          }

        }

        dropAdapter.clear();
        getLeafs(fragmentList);
        dropAdapter.notifyDataSetChanged();


        Toast.makeText(view.getContext(), "Card " + maxId + "has been added", Toast.LENGTH_LONG)
            .show();
        System.out.println(dropItems.size());

      }
    });

    return view;


  }


  private void getLeafs(ArrayList<StoryFragment> fragmentList){
    StoryFragment fragment;
    String spinnerItem;
    leafsList.clear();
    dropAdapter.clear();
    dropItems.clear();
    dropAdapter.notifyDataSetChanged();
    System.out.println("LEAFS SIZE "+leafsList.size());
    System.out.println("DROP ITEMS SIZE: "+ dropItems.size());
    for (int i = fragmentList.size() - 1; i >= 0; i--)
    {
      fragment= fragmentList.get(i);
      if (fragment.getLeft() == -1 && fragment.getRight()==-1)
      {
        System.out.println("FRAGMENT GOTTEN, TEXT: "+fragment.getContent());
        leafsList.add(fragment);
        if(fragment.getContent().length()<20)
        {
          spinnerItem = fragment.getContent();
        }
        else
        {
          spinnerItem = fragment.getContent().substring(0,20);
        }
        System.out.println(spinnerItem);
        dropItems.add(spinnerItem);
        dropAdapter.notifyDataSetChanged();
      }
    }
  }

  private StoryFragment getParentSelected(int chosenID){
    StoryFragment parent;
    parent=leafsList.get(chosenID);
    System.out.println("CHOSEN ID FROM LIST IS: "+chosenID);
    System.out.println("PARENT THAT WE GET IS: "+ parent.getContent());

    for (StoryFragment item:leafsList
         ) {
      System.out.println("LEAFS:"+item.getContent());

    }


    return parent;
  }

  private void addSplit(StoryFragment parent){
    StoryFragment start = new StoryFragment();
    System.out.println("SPLIT PARENT IS: "+parent.getContent());
    if (buttonLeft.isChecked()) {

      parent.setLeft(maxId);
      int id = parent.getPosition();
      fragmentList.set(id,parent);

      start.setPosition(maxId);
      start.setLeft(-1);
      start.setRight(-1);
      start.setContent(fragmentTextString);
      fragmentList.add(start);
      maxId++;
      fragmentText.setText("");

    }
    else {
      System.out.println("SPLIT PARENT IS: "+parent.getContent());
      parent.setRight(maxId);
      int id = parent.getPosition();
      fragmentList.set(id,parent);

      start.setPosition(maxId);
      start.setLeft(-1);
      start.setRight(-1);
      start.setContent(fragmentTextString);
      fragmentList.add(start);
      maxId++;
      fragmentText.setText("");

      }
    }



  private void addSingle(){
    StoryFragment start = new StoryFragment();
    fragmentTextString = fragmentText.getText().toString();

    if (maxId == 0) {
      start.setPosition(maxId);
      start.setLeft(-1);
      start.setRight(-1);
      start.setContent(fragmentTextString);
      fragmentList.add(start);
      maxId++;
      fragmentText.setText("");
    } else {

      StoryFragment parent = getParentSelected(dropdownSelection);
      System.out.println("SINGLE PARENT IS: "+parent.getContent());
      parent.setLeft(maxId);
      parent.setRight(maxId);
      int id = parent.getPosition();
      fragmentList.set(id,parent);

      start.setPosition(maxId);
      start.setLeft(-1);
      start.setRight(-1);
      start.setContent(fragmentTextString);
      fragmentList.add(start);
      maxId++;
      fragmentText.setText("");
    }

  }

}
