package lt.viko.eif.jailgiants.storygenerator.Activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.lorentzos.flingswipe.SwipeFlingAdapterView;

import java.util.ArrayList;
import java.util.List;

import lt.viko.eif.jailgiants.storygenerator.Models.Story;
import lt.viko.eif.jailgiants.storygenerator.Models.StoryFragment;
import lt.viko.eif.jailgiants.storygenerator.R;

public class PlayActivity extends AppCompatActivity {

  private ArrayList<String> al;
  private ArrayAdapter<String> arrayAdapter;
  protected int id = 0;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_play);
    Story story = (Story) getIntent().getSerializableExtra("PassedObject");
    final List contents = story.getStoryFragments();
    al = new ArrayList<>();
    StoryFragment currentItem = (StoryFragment) contents.get(id);
    al.add(currentItem.getContent());
    al.add("");

    arrayAdapter = new ArrayAdapter<>(this, R.layout.item, R.id.helloText, al);

    SwipeFlingAdapterView flingContainer = findViewById(R.id.frame);

    flingContainer.setAdapter(arrayAdapter);
    flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
      @Override
      public void removeFirstObjectInAdapter() {
        Log.d("LIST", "removed object!");
        al.remove(0);
        arrayAdapter.notifyDataSetChanged();
      }

      @Override
      public void onLeftCardExit(Object dataObject) {
        StoryFragment current = new StoryFragment();
        current.setLeft(-1);
        if (id >= 0 && id < contents.size() - 1) {
          current = (StoryFragment) contents.get(id);
        }
        int idLeft = current.getLeft();
        if (idLeft != -1) {
          setLeft(id, contents);
        } else {
          outOfCards();
        }
      }

      @Override
      public void onRightCardExit(Object dataObject) {
        StoryFragment current = new StoryFragment();
        current.setRight(-1);

        if (id >= 0 && id < contents.size() - 1) {
          current = (StoryFragment) contents.get(id);
        }
        int idRight = current.getRight();
        if (idRight != -1) {
          setRight(id, contents);
        } else {
          outOfCards();
        }
      }

      @Override
      public void onAdapterAboutToEmpty(int itemsInAdapter) {
      }

      @Override
      public void onScroll(float scrollProgressPercent) {
      }
    });

    flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
      @Override
      public void onItemClicked(int itemPosition, Object dataObject) {
      }
    });

  }

  public void setLeft(int currentId, List contents) {
    StoryFragment current = (StoryFragment) contents.get(currentId);

    id = current.getLeft();
    if (id == -1) {
      outOfCards();
    }
    if (id + 1 > contents.size()) {
      outOfCards();
    }

    StoryFragment foundCard = getNewCard(id, contents);
    if (foundCard.getContent().equals("") || foundCard.getContent() == null) {
      System.out.println("ENTERED THE IF, NULL FOUND IN LEFT");
      outOfCards();
    }
    al.set(0, foundCard.getContent());
    al.add("");
    arrayAdapter.notifyDataSetChanged();
  }

  public void setRight(int currentId, List contents) {
    StoryFragment current = (StoryFragment) contents.get(currentId);
    id = current.getRight();
    if (id == -1) {
      outOfCards();
    }
    StoryFragment foundCard = getNewCard(id, contents);
    if (foundCard.getContent().equals("") || foundCard.getContent() == null) {
      outOfCards();
    }
    al.set(0, foundCard.getContent());
    al.add("");
    arrayAdapter.notifyDataSetChanged();
  }

  public StoryFragment getNewCard(int searchPosition, List contents) {
    StoryFragment newCard = new StoryFragment();
    StoryFragment emptyCard = new StoryFragment();
    for (int i = 0; i < contents.size(); i++) {
      StoryFragment tmp = (StoryFragment) contents.get(i);
      if (searchPosition == tmp.getPosition()) {
        newCard = tmp;
        break;
      }
    }

    if (newCard.equals(emptyCard)) {
      outOfCards();
    }
    return newCard;
  }

  public void outOfCards() {
    AlertDialog alertDialog = new AlertDialog.Builder(PlayActivity.this).create();
    alertDialog.setTitle("Alert!");
    alertDialog.setMessage("GameOver! Out of cards.");
    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
        new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            finish();
          }
        });
    alertDialog.show();
  }
}
