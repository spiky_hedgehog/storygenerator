package lt.viko.eif.jailgiants.storygenerator.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import java.util.Objects;
import lt.viko.eif.jailgiants.storygenerator.R;

public class Login extends AppCompatActivity {

  EditText mEmailAddress, mPassword;
  Button mLoginButton;
  TextView mGoToRegister;
  ProgressBar mLoginProgressBar;
  FirebaseAuth mAuth;

  private GoogleSignInClient mGoogleSignInClient;
  private int RC_SIGN_IN = 1;

  private LoginButton facebookLoginButton;
  private CallbackManager callbackManager;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);

    mEmailAddress = findViewById(R.id.emailAddress);
    mPassword = findViewById(R.id.password);
    mLoginButton = findViewById(R.id.loginButton);
    mGoToRegister = findViewById(R.id.goToRegister);
    mLoginProgressBar = findViewById(R.id.loginProgressBar);

    mAuth = FirebaseAuth.getInstance();

    FacebookSdk.sdkInitialize(getApplicationContext());

    facebookLoginButton = findViewById(R.id.login_button);
    callbackManager = CallbackManager.Factory.create();
    facebookLoginButton.setReadPermissions("email", "public_profile");

    facebookLoginButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        facebookLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
          @Override
          public void onSuccess(LoginResult loginResult) {
            Toast.makeText(Login.this, "Facebook - good", Toast.LENGTH_SHORT).show();
            handleFacebookAccessToken(loginResult.getAccessToken());
          }

          @Override
          public void onCancel() {
            Toast.makeText(Login.this, "Facebook - cancel", Toast.LENGTH_SHORT).show();
          }

          @Override
          public void onError(FacebookException error) {
            Toast.makeText(Login.this, "Error: " + error, Toast.LENGTH_SHORT).show();
          }
        });
      }
    });

    SignInButton signInButton = findViewById(R.id.googleBtn);
    GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
        .requestIdToken(getString(R.string.default_web_client_id))
        .requestEmail()
        .build();
    mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    signInButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        signIn();
      }
    });

    mLoginButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        String email = mEmailAddress.getText().toString().trim();
        String password = mPassword.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
          mEmailAddress.setError("You need to enter your email to register.");
          return;
        }

        if (password.length() < 6) {
          mPassword.setError("Password must be at least 6 characters long.");
        }

        mLoginProgressBar.setVisibility(View.VISIBLE);

        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(
            new OnCompleteListener<AuthResult>() {
              @Override
              public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                  Toast.makeText(Login.this, "Log in successful", Toast.LENGTH_SHORT).show();
                  startActivity(new Intent(getApplicationContext(), MainActivity.class));
                  finish();
                } else {
                  Toast.makeText(Login.this,
                      "Error!" + Objects.requireNonNull(task.getException()).getMessage(),
                      Toast.LENGTH_SHORT).show();
                }
              }
            });
      }
    });

    mGoToRegister.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        startActivity(new Intent(getApplicationContext(), Register.class));
      }
    });
  }

  private void handleFacebookAccessToken(final AccessToken accessToken) {
    final AuthCredential credential = FacebookAuthProvider.getCredential(accessToken.getToken());

    mAuth.signInWithCredential(credential)
        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
          @Override
          public void onComplete(@NonNull Task<AuthResult> task) {
            if (task.isSuccessful()) {
              updateUI();
              signOut();
              Toast.makeText(Login.this, "Facebook login successful", Toast.LENGTH_SHORT).show();
            } else {
              Toast.makeText(Login.this, "Facebook login failed", Toast.LENGTH_SHORT).show();
              updateUI();
            }
          }
        });
  }

  public void signOut() {
    LoginManager.getInstance().logOut();
  }

  private void signIn() {
    Intent signInIntent = mGoogleSignInClient.getSignInIntent();
    startActivityForResult(signInIntent, RC_SIGN_IN);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == RC_SIGN_IN) {
      Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
      handleSignInResult(task);
    } else {
      callbackManager.onActivityResult(requestCode, resultCode, data);
    }
  }

  private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
    try {
      GoogleSignInAccount acc = completedTask.getResult(ApiException.class);
      Toast.makeText(Login.this, "Signed In Successfully", Toast.LENGTH_SHORT).show();
      FireBaseGoogleAuth(acc);
    } catch (ApiException e) {
      Toast.makeText(Login.this, "Sign In Failed", Toast.LENGTH_SHORT).show();
      FireBaseGoogleAuth(null);
    }
  }

  private void FireBaseGoogleAuth(GoogleSignInAccount acct) {
    if (acct != null) {
      AuthCredential authCredential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
      mAuth.signInWithCredential(authCredential)
          .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
              if (task.isSuccessful()) {
                Toast.makeText(Login.this, "Successful", Toast.LENGTH_SHORT).show();
                updateUI();
              } else {
                Toast.makeText(Login.this, "Failed", Toast.LENGTH_SHORT).show();
                updateUI();
              }
            }
          });
    } else {
      Toast.makeText(Login.this, "acc failed", Toast.LENGTH_SHORT).show();
    }
  }

  private void updateUI() {
    GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
    if (account != null) {
      startActivity(new Intent(getApplicationContext(), MainActivity.class));
      finish();
    }
  }

}
