package lt.viko.eif.jailgiants.storygenerator.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity

import android.os.Bundle
import android.os.Handler
import lt.viko.eif.jailgiants.storygenerator.R

class SplashActivity : AppCompatActivity() {

    private val splashTime = 2000L
    private lateinit var myHandler: Handler


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        myHandler = Handler()

        myHandler.postDelayed({
            redirectToLogin()
        }, splashTime)
    }

    private fun redirectToLogin() {
        val loginIntent = Intent(applicationContext, Login::class.java)
        startActivity(loginIntent)
        finish()
    }

}
